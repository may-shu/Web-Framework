package com.paras.framework.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * Access Logging Filter to log every request onto the console.
 
 * @author Paras.
 */
public class AccessLoggingFilter implements Filter{
    
    private static Logger LOGGER = Logger.getLogger( AccessLoggingFilter.class );
    
    @Override
    public void init( FilterConfig config ) {}
    
    @Override
    public void destroy() {}
    
    @Override
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws ServletException, IOException {
        HttpServletRequest httpRequest = ( HttpServletRequest ) request;
		LOGGER.info( "Request From " + getClientIpAddress( httpRequest ) + " Requesting " + httpRequest.getRequestURI() );
		
		chain.doFilter( request, response );
    }
    
    private static final String[] HEADERS_TO_TRY = { 
    	    "X-Forwarded-For",
    	    "Proxy-Client-IP",
    	    "WL-Proxy-Client-IP",
    	    "HTTP_X_FORWARDED_FOR",
    	    "HTTP_X_FORWARDED",
    	    "HTTP_X_CLUSTER_CLIENT_IP",
    	    "HTTP_CLIENT_IP",
    	    "HTTP_FORWARDED_FOR",
    	    "HTTP_FORWARDED",
    	    "HTTP_VIA",
    	    "REMOTE_ADDR" };

    	public static String getClientIpAddress(HttpServletRequest request) {
    	    for (String header : HEADERS_TO_TRY) {
    	        String ip = request.getHeader(header);
    	        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
    	            return ip;
    	        }
    	    }
    	    return request.getRemoteAddr();
    	}
    
}
