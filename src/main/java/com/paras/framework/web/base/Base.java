package com.paras.framework.web.base;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Base class that provides a utility function called toJson.
 * @author Paras
 *
 */
public abstract class Base {
	
	private static Logger LOGGER = Logger.getLogger( Base.class );
	
	public String toJSON() {

		try{
			
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString( this );
			
		}catch( JsonGenerationException ex ) {
			
			LOGGER.error( ex.getMessage() );
			return null;
			
		}catch( JsonMappingException ex ) {
			
			LOGGER.error( ex.getMessage() );
			return null;
			
		}catch( IOException ex ) {
			
			LOGGER.error( ex.getMessage() );
			return null;
			
		}		
	}
}
