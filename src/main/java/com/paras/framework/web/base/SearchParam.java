package com.paras.framework.web.base;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Class to model a search operations' basic parameters.
 * @author Paras.
 */
public abstract class SearchParam {
	
	private static Logger LOGGER = Logger.getLogger( SearchParam.class );
	
	/**
	 * Current Page of the search result.
	 */
	protected int currentPage;
	
	/**
	 * Number of results in page.
	 */
	protected int pageSize;
	
	/**
	 * Text to enable omni search.
	 */
	protected String text;
	
	/**
	 * Some Search Procedures require that you passed logged in id along with.
	 */
	protected int loggedIn;
	
	public int getLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(int loggedIn) {
		this.loggedIn = loggedIn;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public SearchParam() {
		
	}
	
	public SearchParam( int currentPage, int pageSize ) {
		this.currentPage = currentPage;
		this.pageSize = pageSize;
	}
	
	/**
	 * To check is search param is empty.
	 * @return
	 */
	public abstract boolean isEmpty();
	
	/**
	 * To check if search is simple.
	 */
	public boolean isSimpleSearch() {
		if( isEmpty() ) {
			return false;
		} else {
			
			return StringUtils.isNotBlank( text );
			
		}
	}
	
	/**
	 * To check if search is advanced.
	 */
	public abstract boolean isAdvancedSearch();
	
	public boolean isFreshSearch() {
		return currentPage == 1;
	}
	
	public String toJSON() {

		try{
			
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString( this );
			
		}catch( JsonGenerationException ex ) {
			
			LOGGER.error( ex.getMessage() );
			return null;
			
		}catch( JsonMappingException ex ) {
			
			LOGGER.error( ex.getMessage() );
			return null;
			
		}catch( IOException ex ) {
			
			LOGGER.error( ex.getMessage() );
			return null;
			
		}		
	}
}
