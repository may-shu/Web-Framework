package com.paras.framework.web.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * This class will read a property file and read its content.
 * @author Paras
 *
 */
public class PropertyFileReader {
	
	private static Logger LOGGER = Logger.getLogger( PropertyFileReader.class );	
	/**
	 * Read value of key from file.
	 * @param fileName Name of file to read.
	 * @param key Key to be read.
	 * @return Value.
	 */
	public static String read( String fileName, String key ) {
		
		LOGGER.debug( "In PropertyFileReader | Starting Execution of read");
		
		LOGGER.debug( "In PropertyFileReader | File to read " + fileName );
		LOGGER.debug( "In PropertyFileReader | Key to retrieve " + key );
		
		String value = null;
		
		try{
			if( !fileName.startsWith( "/" )) {
				fileName = "/" + fileName;
			}
			
			Properties properties = new Properties();
			InputStream in = PropertyFileReader.class.getClassLoader().getResourceAsStream( fileName );
			
			if( in != null ) {
				properties.load( in );
				value = properties.getProperty( key );
				
				LOGGER.debug( "In PropertyFileReader | For key : " + key + ", retrieved value is " + value );
			}
		} catch ( IOException ex ) {
			LOGGER.error( "In PropertyFileReader | Caught IOException | " + ex.getMessage(), ex);
		}
		
		LOGGER.debug( "In PropertyFileReader | Finished Execution of read");
		
		return value;
		
	}
	
}
