package com.paras.framework.web.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Request Utility class to provide request related utility tasks.
 * @author Gaurav
 *
 */
public class RequestUtil {

	public static String getContextRoot() {
        ServletRequestAttributes attributes = ( ServletRequestAttributes ) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        
        return request.getContextPath();
	}
	
}
